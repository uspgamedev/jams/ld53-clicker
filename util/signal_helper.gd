class_name SignalHelper extends Object

static func safe_connect(
	signal_obj: Signal,
	method: Callable,
	flags := 0
):
	if not signal_obj.is_connected(method):
		#warning-ignore:return_value_discarded
		signal_obj.connect(method, flags)

static func safe_disconnect(
	signal_obj: Signal,
	method: Callable,
):
	if signal_obj.is_connected(method):
		signal_obj.disconnect(method)

