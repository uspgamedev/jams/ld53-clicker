class_name AutoSignalConnector extends Node

@export var watched_groups: Array[String]
@export var signal_name: String

var _target_method: Callable

func traverse_and_connect(node: Node):
	_connect_signal(node)
	for child in node.get_children():
		traverse_and_connect(child)

func watch(sub_tree: Node, target_method: Callable):
	_target_method = target_method
	SignalHelper.safe_connect(get_tree().node_added, self._connect_signal)
	traverse_and_connect(sub_tree)

func _connect_signal(node: Node):
	for group in watched_groups:
		if node.is_in_group(group):
			SignalHelper.safe_connect(node[signal_name], _target_method)
			return
