# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 1.0.2

- Fix Gym action working when it shouldn't
- Don't leave reset button in such an easy place to miss-click
- Fix bee names
- Tweak BeeFy behavior for better speed and balancing
- Fix honey-making bee no being animated
- Fix some bees' wing animation

## 1.0.1

- Fix all missing sound effects
- Fix soundtrack mixing
- Fix BeeFy attacking animation

## 1.0.0

- Initial submission to Ludum Dare 53
