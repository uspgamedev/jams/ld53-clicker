class_name Effect
extends RefCounted

var traits := {}

# Traits schema
#	- tap: boolean
#	- tick: float
#	- [resource]_add_base: float
#	- [resource]_bonus_flat: float
#	- [resource]_bonus_percent: float
#	- [resource]_absolute: float
#	- [resource]_base: float
#	- [resource]_bonus_flat: float
#	- [resource]_bonus_percent: float
#	- [resource]_absolute: float

func _init(initial_traits: Dictionary):
	traits = initial_traits.duplicate()


static func tap(position: Vector2) -> Effect:
	return Effect.new({tap = true, tap_position = position})


static func tick(delta: float) -> Effect:
	return Effect.new({tick = delta})


static func spawn_bee(bee_type: GameState.BeeType) -> Effect:
	return Effect.new({bee_to_spawn = bee_type})
	
	
static func retire_bee(bee: Bee) -> Effect:
	return Effect.new({bee_to_retire = bee})
	

static func produce_bee(bee_type: GameState.BeeType, beesource: GameState.Beesource, price: float):
	return Effect.new({bee_to_produce = bee_type, beesource = beesource, price = price})


static func expand_hive(expansion: Expansion, wax_cost: float = 0.0) -> Effect:
	return Effect.new({
		expansion = expansion,
		wax_lose_base = wax_cost
	})


static func transform_beesource(beesource_to_consume : GameState.Beesource, value_to_consume: float,
								beesource_to_produce : GameState.Beesource, value_to_produce: float):
	var initial_traits = {
		transform_beesource = true
	}
	var consumed_name := GameState.VALID_RESOURCE_NAMES[beesource_to_consume]
	var produced_name := GameState.VALID_RESOURCE_NAMES[beesource_to_produce]
	initial_traits[produced_name + "_add_base"] = value_to_produce
	initial_traits[consumed_name + "_lose_base"] = value_to_consume
	return Effect.new(initial_traits)


static func tap_attack(threat: Threat) -> Effect:
	return Effect.new({
		tap_attack = true,
		damage = true,
		damage_target = threat,
		damage_base = Constants.tap_attack_damage_base
	})


static func send_bee_to_attack(bee: Bee, threat: Threat, callback: Callable) -> Effect:
		return Effect.new({
		follow = true,
		to_follow = bee,
		follow_target = threat,
		speed = Constants.bee_speed_base,
		callback = callback
	})
	
static func bee_attack(bee: Bee, threat: Threat) -> Effect:
		return Effect.new({
		bee_attack = bee,
		damage = true,
		damage_target = threat,
		damage_base = Constants.bee_attack_damage_base
	})


func prevent() -> Effect:
	traits.prevented = true
	return self


func is_prevented() -> bool:
	return traits.get("prevented", false)


func stack_trait(name: String, value: float) -> Effect:
	traits[name] = traits.get(name, 0.0) + value
	return self


func merge_trait(name: String, values: Dictionary) -> Effect:
	var current_trait = traits.get(name, {})
	current_trait.merge(values)
	traits[name] = current_trait
	return self


func use_universal_formula(prefix: String) -> Effect:
	if traits.has(prefix + "_base"):
		var base = traits.get(prefix + "_base")
		var flat = traits.get(prefix + "_bonus_flat", 0.0)
		var percent = traits.get(prefix + "_bonus_percent", 0.0)
		var exponent = traits.get(prefix + "_bonus_exp", 1.0)
		var absolute = (base + flat) * pow(1 + percent, exponent)
		traits[prefix + "_absolute"] = absolute
	return self
