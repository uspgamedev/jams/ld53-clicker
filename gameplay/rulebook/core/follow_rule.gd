extends ApplyRule

func _handle(effect: Effect, _game_state: GameState):
	if effect.traits.get("follow", false):
		var to_follow = effect.traits.to_follow as Node
		var target = effect.traits.follow_target as Node
		var speed = effect.traits.speed
		var callback = effect.traits["callback"]
		var duration = effect.traits.get("duration")
		
		if  to_follow == null or target == null or \
				not to_follow.is_inside_tree() or not target.is_inside_tree():
			effect.traits.prevented = true
			return
				
		if duration == null:
			duration = (to_follow.position - target.position).length()/speed
			
		var follow = Follow.new(to_follow, target, speed, duration, callback)
		add_child(follow)

class Follow extends Node:
	var to_follow : Node2D
	var target : Node2D
	var speed : float
	var duration : float
	var callback : Callable
	
	var time := 0.0
	
	func _init(_to_follow: Node2D, _target: Node2D, _speed: float, _duration, _callback: Callable):
		to_follow = _to_follow
		target = _target
		speed = _speed
		duration = _duration
		callback = _callback
		time = 0.0
		
	func _process(delta: float):
		time += delta
			
		if time >= duration or target == null or to_follow == null or \
				(target.global_position - to_follow.global_position).length() < 1.0:
			queue_free()
			callback.call_deferred()
			return
			
		var diff = target.global_position - to_follow.global_position
		to_follow.global_position += delta * speed * diff

