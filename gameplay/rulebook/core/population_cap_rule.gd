extends ModifyRule


func _handle(effect: Effect, _game_state: GameState):
	if effect.traits.has("bee_to_produce"):
		var cap := effect.traits.get("bee_population_cap", 0) as int
		if get_tree().get_nodes_in_group(Bee.BEE_GROUP).size() >= cap:
			effect.prevent()
