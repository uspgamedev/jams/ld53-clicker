extends ModifyRule

func _handle(effect: Effect, game_state: GameState):
	if effect.traits.has("tick"):
		var flowers = game_state.flowers
		var delta := effect.traits.tick as float
		effect.traits.pollen_add_base = Constants.flower_pollen_per_sec_base \
										* flowers * delta
