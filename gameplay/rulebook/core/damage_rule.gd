extends ApplyRule

func _handle(effect: Effect, _game_state: GameState):
	if effect.traits.get("damage", false):
		var damage := effect.traits.get("damage_absolute", 0.0) as float
		var target := effect.traits.get("damage_target", null) as Threat
		if target and damage > 0:
			target.hp = max(target.hp - damage, 0)
