extends ModifyRule

@export var damage_prefixes: Array[String]

func _handle(effect: Effect, _game_state: GameState):
	for damage_prefix in damage_prefixes:
		effect.use_universal_formula(damage_prefix)
