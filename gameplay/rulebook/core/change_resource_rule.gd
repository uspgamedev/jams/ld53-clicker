class_name ChangeResourceRule
extends ApplyRule

func _handle(effect: Effect, game_state: GameState):
	for resource_name in GameState.VALID_RESOURCE_NAMES:
		var current_value = game_state.get(resource_name)
		var add_absolute = effect.traits.get("%s_add_absolute" % resource_name, 0)
		var lose_absolute = effect.traits.get("%s_lose_absolute" % resource_name, 0)
		var change_absolute = add_absolute - lose_absolute
		game_state.set(resource_name, current_value + change_absolute)
