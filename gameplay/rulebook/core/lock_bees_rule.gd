extends ModifyRule

func _handle(effect: Effect, _game_state: GameState):
	if effect.traits.has("bee_to_spawn"):
		var allowed = effect.traits.get("allow_unlocked", false)
		if not effect.traits.get("bee_unlocked", false) and not allowed:
			effect.prevent()
