extends ModifyRule

@export var affected_values: Array[String]

const CHANGE_TYPES = ["add", "lose"]

func _handle(effect: Effect, _game_state: GameState):
	for resource_name in GameState.VALID_RESOURCE_NAMES:
		for change_type in CHANGE_TYPES:
			var prefix := "%s_%s" % [resource_name, change_type]
			effect.use_universal_formula(prefix)
