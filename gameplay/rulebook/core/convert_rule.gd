extends ApplyRule

@export var affected_values: Array[String]


func _handle(effect: Effect, game_state: GameState):
		if effect.traits.has("convert"):
			var price = effect.traits.price
			var bee_to_lose = effect.traits.bee_to_lose
			var bee_to_add = effect.traits.bee_to_add
			var flag = false
			
			if game_state.honey < price:
				return
				
			for bee in get_tree().get_nodes_in_group("BEE"):
				if bee.type == bee_to_lose:
					flag = true
					Rulebook.resolve(Effect.retire_bee(bee), game_state)
					break 
			if flag == false:
				return
		
			game_state.honey -= price
			var fx = Effect.spawn_bee(bee_to_add)
			fx.traits.allow_unlocked = true
			Rulebook.resolve(fx, game_state)
		
		
		
