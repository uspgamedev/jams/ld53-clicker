extends ApplyRule

@export var affected_values: Array[String]


func _handle(effect: Effect, game_state: GameState):
	if effect.traits.has("transform_beesource"):
		var price = effect.traits.value_to_consume
		var produced_value = effect.traits.value_to_produce 
		
		match effect.traits.beesource_to_consume as GameState.Beesource:
			GameState.Beesource.POLLEN:
				if game_state.pollen < price:
					return
				game_state.pollen -= price
			GameState.Beesource.FLOWERS:
				if game_state.flowers < price:
					return
				game_state.flowers -= int(price)
			GameState.Beesource.HONEY:
				if game_state.honey < price:
					return
				game_state.honey -= price
			GameState.Beesource.WAX:
				if game_state.wax < price:
					return
				game_state.wax -= price
				
		match effect.traits.beesource_to_produce as GameState.Beesource:
			GameState.Beesource.POLLEN:
				game_state.pollen += produced_value
			GameState.Beesource.FLOWERS:
				game_state.flowers += int(produced_value)
			GameState.Beesource.HONEY:
				game_state.honey += produced_value
			GameState.Beesource.WAX:
				game_state.wax += produced_value
		
		
		
