class_name Rule
extends Node

# Rule groups
const MODIFY_RULE := "MODIFY_RULE"
const APPLY_RULE := "APPLY_RULE"

@export var enabled := true

func handle(effect: Effect, game_state: GameState):
	if enabled:
		_handle(effect, game_state)

func _handle(_effect: Effect, _game_state: GameState):
	push_error("%s: called abstract method Rule._handle()" % get_path())
