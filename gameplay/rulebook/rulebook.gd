extends Node

const RULE_ORDER := [Rule.MODIFY_RULE, Rule.APPLY_RULE]

func resolve(effect: Effect, game_state: GameState) -> Effect:
	for rule_group in RULE_ORDER:
		for rule in get_tree().get_nodes_in_group(rule_group):
			if rule is Rule:
				rule.handle(effect, game_state)
		if effect.is_prevented():
			break
	if not effect.is_prevented():
		game_state.effect_resolved.emit(effect)
	return effect


func preview(effect: Effect, game_state: GameState) -> Effect:
	for rule in get_tree().get_nodes_in_group(Rule.MODIFY_RULE):
		if rule is Rule:
			rule.handle(effect, game_state)
	return effect
