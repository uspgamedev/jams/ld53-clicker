class_name Threat
extends Node2D

signal arrived(threat: Threat)

@export var hp_max := 1
@export var arrival_time := 10
@export var initial_progress := 0.0

# Injected upon creation
var game_state: GameState

# Updated elsewhere
var origin_position: Vector2
var target_position: Vector2
var hp := 0

var _arrived := false

func _init():
	hp = hp_max

func _ready():
	var wait_time = arrival_time * (1.0 - initial_progress)
	if wait_time > 0:
		$Timer.wait_time = wait_time
		$Timer.start()
		await $Timer.timeout
	_arrived = true
	arrived.emit(self)


func _process(_delta):
	var progress := get_progress()
	position = lerp(origin_position, target_position, progress)
	$HPBar.value = 100.0 * hp / hp_max


func _physics_process(_delta):
	if hp <= 0:
		die()


func get_progress() -> float:
	return 1.0 - $Timer.time_left / arrival_time


func die():
	# TODO vfx
	queue_free()


func _on_input(event):
	if event is InputEventScreenTouch:
		if event.is_pressed() and _arrived:
			Rulebook.resolve(Effect.tap_attack(self), game_state)
