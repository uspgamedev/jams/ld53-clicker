extends Node

signal effect_created(effect: Effect)

var arrived := false

func _physics_process(delta):
	if arrived:
		effect_created.emit(Effect.new({
			bear_tick = true,
			honey_lose_base = Constants.bear_honey_consume_rate * delta
		}))


func _on_bear_arrived(_threat):
	arrived = true
