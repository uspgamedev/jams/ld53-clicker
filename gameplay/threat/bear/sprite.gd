extends AnimatedSprite2D

var arrived := false
var tween

func _ready():
	tween = create_tween().set_trans(Tween.TRANS_SINE).set_ease(Tween.EASE_IN_OUT)
	tween.tween_property(self, "rotation", -PI/15, 0.3)
	tween.tween_property(self, "rotation", PI/15, 0.3)
	tween.set_loops()


func _on_bear_arrived(_threat):
	tween.kill()
	rotation = 0
	play("default")
