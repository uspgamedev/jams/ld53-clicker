extends Control

signal threat_appeared
signal threat_over

@export var threat_pool: Array[PackedScene]
@export var delay := 5
@export var margin_left := 100
@export var margin_right := 100
@export var height := 200
@export var slide_speed := 100

@export_category("Extra")
@export var death_sfx_scn: PackedScene

# Injected
var bg: Background
var hive: Hive

var _game_state: GameState
var _current_threat: Threat

func _game_state_ready(game_state: GameState, save_data):
	_game_state = game_state
	if save_data:
		var current_threat_scn = save_data.get("threat_current", null)
		if current_threat_scn:
			_current_threat = load(current_threat_scn).instantiate()
			_current_threat.hp = save_data.threat_current_hp
			_current_threat.initial_progress = save_data.threat_current_progress
			# GAMBZ
			await get_tree().process_frame
			await get_tree().process_frame
			_spawn_threat.call_deferred()
		else:
			_schedule_threat(save_data.threat_delay)
	else:
		_schedule_threat(delay)


func _save_state(save_data: Dictionary):
	if _current_threat.is_inside_tree():
		save_data.threat_current = _current_threat.scene_file_path
		save_data.threat_current_hp = _current_threat.hp
		save_data.threat_current_progress = _current_threat.get_progress()
	else:
		save_data.threat_delay = $Timer.time_left


func _schedule_threat(wait_time):
	threat_over.emit()
	_current_threat = threat_pool.pick_random().instantiate()
	_current_threat.hp = _current_threat.hp_max
	$Timer.wait_time = wait_time
	$Timer.start.call_deferred()


func _spawn_threat():
	_current_threat.game_state = _game_state
	_game_state.add_child(_current_threat)
	_current_threat.tree_exited.connect(func():
		var sfx := death_sfx_scn.instantiate() as Node2D
		sfx.position = _current_threat.position
		_game_state.add_child(sfx)
		_schedule_threat(delay)
	)
	threat_appeared.emit()


func _process(delta):
	if _current_threat:
		var origin_pos := bg.scroll_max - margin_right
		var target_pos := hive.get_edge_position().x + margin_left
		var bottom := position.y
		if _current_threat.is_inside_tree():
			_current_threat.origin_position = _current_threat.origin_position \
				.move_toward(
					Vector2(origin_pos, bottom),
					slide_speed * delta
				)
			_current_threat.target_position = _current_threat.target_position \
				.move_toward(
					Vector2(target_pos, bottom),
					slide_speed * delta
				)
		else:
			_current_threat.origin_position = Vector2(origin_pos, bottom)
			_current_threat.target_position = Vector2(target_pos, bottom)


