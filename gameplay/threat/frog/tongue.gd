extends Timer

var _frog: Threat

func _on_arrived(threat: Threat):
	_frog = threat
	start()


func _on_timeout():
	var bees := get_tree().get_nodes_in_group(Bee.BEE_GROUP)
	bees = bees.filter(func(bee): return bee.type != GameState.BeeType.BeeFy )
	
	if bees.size() > 0:
		var bee = bees.pick_random()
		bee.killed = true
		var fx := Rulebook.resolve(Effect.retire_bee(bee), _frog.game_state)
		$TongueSFX.play()
		if fx.is_prevented():
			await get_tree().create_timer(0.1).timeout
			bee.killed = false
