class_name Bee extends Node2D

signal retired(bee: Node)
signal attacked()

const BEE_GROUP := "BEE"

@export var type := GameState.BeeType.Beelivery
@export var speed := 1.0
@export var retirement_time := 100.0

@export_category("Textures")
@export var body_textures : Array[Texture]

@export_category("Behaviors")
@export var behavior_scripts : Array[Script]


@onready var initial_position := self.position 

var phi = 0.0
var killed := false

@export var SPACE_X := 200
@export var SPACE_Y := 80 


var flight_displacement = Vector2()
var flight_amplitude = Vector2() 
var phase = 0.0
var wandering = true

func spawn(bee_type : GameState.BeeType, max_position_x : float):
	self.type = bee_type
	
	flight_amplitude.x = randf_range((Constants.WIDTH - SPACE_X)/4, max_position_x - SPACE_X)
#	flight_amplitude.y = randf_range((Constants.HEIGHT - SPACE_Y)/8, (Constants.HEIGHT - SPACE_Y)/5)
#	flight_displacement.y = randf_range(0 + SPACE_Y, Constants.HEIGHT - 200 - SPACE_Y)
	flight_amplitude.y = randf_range(Constants.HEIGHT/8, Constants.HEIGHT/5)
	flight_displacement.y = randf_range(Constants.HEIGHT/7, Constants.HEIGHT/2 - SPACE_Y)
	flight_displacement.x = randf_range(SPACE_X, max_position_x - flight_amplitude.x)
	phase = randf_range(0, 2*PI)
	
	self.animate.call_deferred()
	
	$Timer.wait_time = retirement_time
	$Timer.start.call_deferred()
	
	$Body.texture = body_textures[type]
	
	for behavior in $Behaviors.get_children():
		behavior.disable()
	
	match type:
		GameState.BeeType.Beelivery:
			%Production.enable(type)
			self.animate_wing.bind($BackWings, 0.0, PI/8).call_deferred()
			self.animate_wing.bind($FrontWings, PI, PI/8).call_deferred()
		GameState.BeeType.WorkerBee:
			%Production.enable(type)
			self.animate_wing.bind($BackWings, 0.0, PI/8).call_deferred()
			self.animate_wing.bind($FrontWings, PI, PI/8).call_deferred()
		GameState.BeeType.GardenBee:
			%DiscreteProduction.enable(type)
			self.animate_wing.bind($BackWings, 0.0, PI/8).call_deferred()
			self.animate_wing.bind($FrontWings, PI, PI/8).call_deferred()
		GameState.BeeType.BeeCooker:
			%Production.enable(type)
			self.animate_wing.bind($BackWings, 0.0, PI/8).call_deferred()
			self.animate_wing.bind($FrontWings, PI, PI/8).call_deferred()
		GameState.BeeType.MotorBee:
			%Production.enable(type)
			self.animate_wing.bind($BackWings, 0.0, PI/8).call_deferred()
			self.animate_wing.bind($FrontWings, PI, PI/8).call_deferred()
			
			$BackWings.visible = false
			$Motorcycle.visible = true
			%MotorBeeBackWings.visible = true
			%MotorBeeFrontWings.visible = true
			$FrontWings.visible = false
			self.animate_wing.bind(%MotorBeeBackWings, 0.0, PI/12).call_deferred()
			self.animate_wing.bind(%MotorBeeFrontWings, PI, PI/12).call_deferred()
			self.animate_biker.call_deferred()
			$MotorcycleTimer.start.call_deferred()
#			$Body.rotation = -PI/4
			$Motorcycle.rotation = -PI/4 + 0.1
		GameState.BeeType.BeeFy:
			$BackWings.visible = false
			$BeeFyWings.visible = true
			$FrontWings.visible = false
			self.animate_wing.bind($BeeFyWings, 0.0 , PI/8).call_deferred()
			
			$HurtBox.area_entered.connect(_on_hurt_box_area_entered)
		_:
			self.animate_wing.bind($BackWings, 0.0, PI/8).call_deferred()
			self.animate_wing.bind($FrontWings, PI, PI/8).call_deferred()
	

func _process(_delta):
	if wandering:
		self.position.x = flight_displacement.x + flight_amplitude.x/2.0 + (flight_amplitude.x/2.0)*sin(-PI + phi + phase)
		self.position.y = flight_displacement.y + flight_amplitude.y/2.0 + (flight_amplitude.y/2.0)*sin(-PI + 2*(phi+phase)) 
		
		var theta = phi + phase
		while theta > 2 * PI:
			theta -= 2 * PI
		if (theta >= PI/3 and theta <= 3*PI/2):
			if self.scale.x > 0.0:
				self.scale.x *= -1
		elif self.scale.x < 0.0:
			self.scale.x *= -1
		
func connect_signals(game_state: GameState):
	self.retired.connect(game_state.prepare_to_retire_bee)
	for behavior in $Behaviors.get_children():
		behavior.connect_signals(game_state)


func get_retirement_time() -> float:
	return $Timer.time_left


func animate():
	var tween = self.create_tween()

	tween.tween_property(self, "phi", 2 * PI + phase, 5.0/speed)
	tween.tween_property(self, "phi", phase, 0.0)
	tween.set_loops()

func animate_wing(wing: Node, theta: float, angle: float):
	
	wing.rotation = angle * (theta/(2*PI))
	
	var tween1 = self.create_tween().set_trans(Tween.TRANS_SINE)
	tween1.tween_property(wing, "rotation", angle , 0.1 * (1 - theta/(2*PI)))
	tween1.tween_property(wing, "rotation", 0.0 , 0.1)
	
	await tween1.finished
	
	var tween_wings = self.create_tween().set_trans(Tween.TRANS_SINE)
	tween_wings.tween_property(wing, "rotation", angle , 0.1)
	tween_wings.tween_property(wing, "rotation", 0.0, 0.1)
	tween_wings.set_loops()


func animate_biker():
	var tween2 = self.create_tween().set_trans(Tween.TRANS_SINE)
	tween2.tween_property($Body, "rotation", -PI/4 , 0.5)
	tween2.tween_property($Body, "rotation", 0.0 , 0.5)
	tween2.set_loops()

func stop_wander():
	wandering = false

func _on_timer_timeout():
	retired.emit(self)

func _on_motorcycle_timer_timeout():
	var tween = self.create_tween().set_trans(Tween.TRANS_SINE)
	tween.tween_property($Motorcycle, "rotation", 0.2 , 1.0)
	tween.tween_property($Motorcycle, "rotation", 0.2 , 0.5)
	tween.tween_property($Motorcycle, "rotation", -PI/4+0.1 , 1.0)


func _on_hurt_box_area_entered(area):
	if area.is_in_group("STING"):
		attacked.emit()
