extends Control

@export var attack_color = Color(0.9, 0.3, 0.3)
@export var attack_sfx_scn: PackedScene

var _game_state : GameState

var has_threat: bool = false

var attacking : bool = false
var threat : Node

func _game_state_ready(game_state: GameState, save_data):
	_game_state = game_state
	if save_data:
		has_threat = save_data.has_threat 
#		$Timer.start.bind(save_data.time_left_for_next_protect).call_deferred()
		
		await get_tree().process_frame
		await get_tree().process_frame
		await get_tree().process_frame

func _save_state(save_data: Dictionary):
	save_data.has_threat = has_threat
#	save_data.time_left_for_next_protect = $Timer.time_left

func _physics_process(_delta):
	try_attack()

func _on_threat_appeared():
	has_threat = true
	await get_tree().process_frame
	await get_tree().process_frame
	await get_tree().process_frame
	threat = get_tree().get_nodes_in_group("THREAT")[0]
	
func _on_threat_over():
	has_threat = false
	threat = null

func try_attack():
	if has_threat and threat != null and attacking == false and $Timer.is_stopped():
		var protectors = get_protectors()
		if protectors.size() > 0:
			attacking = true
			send_bee_to_attack(protectors.front(), Constants.bee_speed_base)
	
func send_bee_to_attack(bee: Bee, _speed: float):
	bee.modulate *= attack_color
	bee.attacked.connect(attack.bind(bee))
	Rulebook.resolve(Effect.send_bee_to_attack(bee, threat, attack.bind(bee)), _game_state)

func attack(bee: Bee):
	var fx := Rulebook.resolve(Effect.bee_attack(bee, threat), _game_state)
	attacking = false
	$Timer.start()
	if not fx.is_prevented():
		var sfx := attack_sfx_scn.instantiate()
		sfx.position = bee.position
		_game_state.add_child(sfx)

func connect_threat_signals(threat_director: Node):
	threat_director.threat_appeared.connect(_on_threat_appeared)
	threat_director.threat_over.connect(_on_threat_over)

func get_protectors() -> Array[Node]:
	var protectors : Array[Node] = []
	for bee in _game_state.get_bees():
		if bee.type == GameState.BeeType.BeeFy:
			protectors.append(bee)
	return protectors
