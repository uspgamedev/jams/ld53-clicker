extends Node

@export var bee_type: GameState.BeeType

var consume_beesource := GameState.Beesource.NONE
var produce_beesource := GameState.Beesource.NONE
var consume_rate := 0.0
var produce_rate := 0.0

func disable():
	set_physics_process(false)
	set_process(false)

func enable(_bee_type: GameState.BeeType):
	self.bee_type = _bee_type
	match bee_type:
		GameState.BeeType.GardenBee:
			consume_rate = Constants.garden_bee_consume_rate
			produce_rate = Constants.garden_bee_produce_rate
			consume_beesource = GameState.Beesource.POLLEN
			produce_beesource = GameState.Beesource.FLOWERS
		_:
			print("Wrong Behavior")
	
	var timer = Timer.new()
	timer.wait_time = 1.0/produce_rate
	timer.timeout.connect(_on_timer_timeout)
	add_child(timer)
	timer.start.bind(1.0/produce_rate).call_deferred()
	timer.paused = false
	timer.autostart = true
	timer.one_shot = false
	
	set_physics_process(true)
	set_process(true)

signal transform_beesource(beesource_to_consume : GameState.Beesource, value_to_consume: float,
								beesource_to_produce : GameState.Beesource, value_to_produce: float, 
								bee_type: GameState.BeeType)

func connect_signals(game_state: GameState):
	self.transform_beesource.connect(game_state.transform_beesource)

func _on_timer_timeout():
	transform_beesource.emit(consume_beesource, consume_rate / produce_rate,
					produce_beesource, 1.0, bee_type)
