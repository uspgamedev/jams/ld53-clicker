extends Node

@export var bee_type: GameState.BeeType

var consume_beesource := GameState.Beesource.NONE
var produce_beesource := GameState.Beesource.NONE
var consume_rate := 0.0
var produce_rate := 0.0

func disable():
	set_physics_process(false)
	set_process(false)

func enable(_bee_type: GameState.BeeType):
	self.bee_type = _bee_type
	match bee_type:
		GameState.BeeType.Beelivery:
			produce_rate = Constants.beelivery_produce_rate
			produce_beesource = GameState.Beesource.POLLEN
		GameState.BeeType.WorkerBee:
			consume_rate = Constants.worker_bee_consume_rate
			produce_rate = Constants.worker_bee_produce_rate
			consume_beesource = GameState.Beesource.POLLEN
			produce_beesource = GameState.Beesource.WAX
		GameState.BeeType.BeeCooker:
			consume_rate = Constants.bee_cooker_consume_rate
			produce_rate = Constants.bee_cooker_produce_rate
			consume_beesource = GameState.Beesource.POLLEN
			produce_beesource = GameState.Beesource.HONEY
		GameState.BeeType.MotorBee:
			consume_rate = Constants.motorbee_consume_rate
			produce_rate = Constants.motorbee_produce_rate
			consume_beesource = GameState.Beesource.HONEY
			produce_beesource = GameState.Beesource.POLLEN
		_:
			print("Wrong Behavior")
			
	set_physics_process(true)
	set_process(true)

signal transform_beesource(beesource_to_consume : GameState.Beesource, value_to_consume: float,
								beesource_to_produce : GameState.Beesource, value_to_produce: float, 
								bee_type: GameState.BeeType)

func _physics_process(delta: float):
	transform_beesource.emit(consume_beesource, consume_rate * delta,
					produce_beesource, produce_rate * delta, bee_type)

func connect_signals(game_state: GameState):
	self.transform_beesource.connect(game_state.transform_beesource)
	
