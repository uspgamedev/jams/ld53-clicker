extends ApplyRule


func _handle(effect: Effect, game_state: GameState):
	if effect.traits.has("bee_to_retire"):
		var bee = effect.traits["bee_to_retire"] as Bee
		game_state.retire_bee.bind(bee).call_deferred()
		
