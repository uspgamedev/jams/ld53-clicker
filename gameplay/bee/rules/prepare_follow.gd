extends ApplyRule

func _handle(effect: Effect, _game_state: GameState):
	if effect.traits.get("follow", false):
		var to_follow = effect.traits.to_follow
		var target = effect.traits.follow_target
		
		if to_follow == null or target == null or \
					not to_follow.is_inside_tree() or not target.is_inside_tree():
			effect.traits.prevented = true
			return
			
		if to_follow is Bee:
			to_follow.stop_wander()
			
			if (target.global_position - to_follow.global_position).x > 0:
				if to_follow.scale.x > 0.0:
					to_follow.scale.x *= -1
			else:
				if to_follow.scale.x > 0.0:
						to_follow.scale.x *= -1

	
