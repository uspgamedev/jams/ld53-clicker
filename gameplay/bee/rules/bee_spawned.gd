extends ApplyRule

@export var bee_types : Array[GameState.BeeType]

signal spawned(bee_type: GameState.BeeType) 

func _handle(effect: Effect, _game_state: GameState):
	if effect.traits.has("bee_to_spawn"):
		var bee_type = effect.traits["bee_to_spawn"] as GameState.BeeType
		
		if bee_types.has(bee_type):
			spawned.emit(bee_type)
			
		
