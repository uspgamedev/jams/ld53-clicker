extends ApplyRule


func _handle(effect: Effect, game_state: GameState):
	if effect.traits.has("bee_to_spawn"):
		var bee_type = effect.traits["bee_to_spawn"] as GameState.BeeType
		game_state.spawn_bee(bee_type)
		
