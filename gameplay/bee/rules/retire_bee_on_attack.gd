extends ApplyRule


func _handle(effect: Effect, game_state: GameState):
	if effect.traits.has("bee_attack"):
		var bee = effect.traits.bee_attack
		Rulebook.resolve(Effect.retire_bee(bee), game_state)
		
