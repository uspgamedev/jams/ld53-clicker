extends ApplyRule


func _handle(effect: Effect, game_state: GameState):
	if effect.traits.has("bee_to_produce"):
		var price = effect.traits["price"] as float
		var beesource = effect.traits["beesource"] as GameState.Beesource
		
		match beesource:
			GameState.Beesource.POLLEN:
				if game_state.pollen < price:
					return
				game_state.pollen -= price
			GameState.Beesource.FLOWERS:
				if game_state.flowers < price:
					return
				game_state.flowers -= int(price)
			GameState.Beesource.HONEY:
				if game_state.honey < price:
					return
				game_state.honey -= price
			GameState.Beesource.WAX:
				if game_state.wax < price:
					return
				game_state.wax -= price
		
		
		var bee_type = effect.traits["bee_to_produce"] as GameState.BeeType
		
		var spawn_effect = func() : Rulebook.resolve(Effect.spawn_bee(bee_type), game_state)
		spawn_effect.call_deferred()
