extends ModifyRule

func _handle(effect: Effect, game_state: GameState):
	for resource_name in GameState.VALID_RESOURCE_NAMES:
		var trait_name: String = resource_name + "_lose_absolute"
		var lose_absolute: float = effect.traits.get(trait_name, 0)
		var current_value: float = game_state.get(resource_name)
		if lose_absolute > current_value:
			effect.traits.prevented = true
