class_name GameState
extends Node2D

const VALID_RESOURCE_NAMES: Array[String] = [
	"pollen",
	"flowers",
	"honey",
	"wax"
]

signal effect_resolved(effect: Effect)
signal beesource_changed(resource: Beesource, value: float)
signal bees_changed(bee_type: BeeType, value: int)

enum Beesource { NONE = -1, POLLEN, FLOWERS, HONEY, WAX }
enum BeeType { Beelivery, WorkerBee, GardenBee, BeeCooker, MotorBee, BeeFy}

@export_category("Scenes")
@export var bee_scene : PackedScene
@export var flower_scene : PackedScene
@export var bee_retire_sfx_scene: PackedScene
@export var bee_die_sfx_scene: PackedScene

@export_category("Beesources")
@export var pollen := 0.0 : set = set_pollen
@export var flowers := 0 : set = set_flowers
@export var honey := 0.0 : set = set_honey
@export var wax := 0.0 : set = set_wax


var bees := [0, 0, 0, 0, 0, 0, 0]
var flowers_nodes : Array[Node] = []
var hive

func _game_state_ready(_self: GameState, save_data):
	if save_data:
		pollen = save_data.pollen
		flowers = save_data.flowers
		honey = save_data.honey
		await get_tree().process_frame
		await get_tree().process_frame
		for bee_data in save_data.bees:
			spawn_bee.call_deferred(bee_data.type, bee_data.retirement_time)


func _save_state(save_data: Dictionary):
	save_data.pollen = pollen
	save_data.flowers = flowers
	save_data.honey = honey
	save_data.wax = wax
	save_data.bees = []
	for child in get_children():
		if child is Bee:
			var bee_data := {
				type = child.type,
				retirement_time = child.get_retirement_time()
			}
			save_data.bees.append(bee_data)


func get_bee(bee: BeeType) -> int:
	return bees[bee]


func spawn_bee(bee_type: BeeType, retirement_time := 0.0):
	var bee := bee_scene.instantiate() as Bee
	bee.spawn(bee_type, hive.get_edge_position().x)
	bee.connect_signals.bind(self).call_deferred()
	
	if retirement_time > 0.0:
		bee.retirement_time = retirement_time
	self.add_child(bee)
	
	bees[bee_type] += 1
	bees_changed.emit(bee_type, bees[bee_type])


func retire_bee(bee: Bee):
	bees[bee.type] -= 1
	bees_changed.emit(bee.type, bees[bee.type])
#	bee.retired.emit(bee)
	
	remove_child.bind(bee).call_deferred()
	var sfx: Node2D
	if bee.killed:
		sfx = bee_die_sfx_scene.instantiate()
	else:
		sfx = bee_retire_sfx_scene.instantiate()
	sfx.position = bee.position
	bee.queue_free()
	add_child(sfx)


func set_pollen(value: float):
	pollen = value
	beesource_changed.emit(Beesource.POLLEN, value)
	return value
	
func set_flowers(value: int):
	while value > flowers:
		var flower := flower_scene.instantiate()
		flower.spawn(flowers_nodes.size())
		flowers_nodes.append(flower)
		(func(): add_child(flower)).call_deferred()
		flowers += 1
		
	while value < flowers:
		var flower = flowers_nodes.pop_back()
		remove_child(flower)
		flower.queue_free()
		flowers -= 1
		
	flowers = value
	beesource_changed.emit(Beesource.FLOWERS, value)
	
	return value


func set_honey(value: float):
	honey = value
	beesource_changed.emit(Beesource.HONEY, value)
	return value 


func set_wax(value: float):
	wax = value
	beesource_changed.emit(Beesource.WAX, value)
	return value 


func prepare_to_retire_bee(bee: Bee): # Private
	Rulebook.resolve.bind(Effect.retire_bee(bee), self).call_deferred()
	

func transform_beesource(beesource_to_consume : GameState.Beesource, value_to_consume: float,
								beesource_to_produce : GameState.Beesource, value_to_produce: float, 
								_bee_type: GameState.BeeType):
		Rulebook.resolve(Effect.transform_beesource(beesource_to_consume, value_to_consume,
								beesource_to_produce, value_to_produce), self)


func has_threat() -> bool:
	return get_tree().get_nodes_in_group("THREAT").size() != 0

func get_bees() -> Array[Node]:
	return get_tree().get_nodes_in_group("BEE")	
