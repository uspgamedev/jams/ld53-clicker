class_name SaveState
extends Resource

const SAVE_STATE_PATH := "user://savestate.json"

static func load_data():
	var savefile := FileAccess.open(SAVE_STATE_PATH, FileAccess.READ)
	if not savefile:
		print("No save data found, beginning new game...")
		return null
	var content := savefile.get_as_text()
	savefile.close()
	print("Loaded game from %s" % SAVE_STATE_PATH)
	
	return JSON.parse_string(content)

static func save_data(data: Dictionary):
	var savefile := FileAccess.open(SAVE_STATE_PATH, FileAccess.WRITE)
	if not savefile:
		push_error("Failed to save game to %s (err %d)" % [
			SAVE_STATE_PATH,
			FileAccess.get_open_error()
		])
		return
	var content := JSON.stringify(data)
	savefile.store_string(content)
	savefile.close()
	print("Saved game to %s" % SAVE_STATE_PATH)


static func clear_data():
	DirAccess.remove_absolute(SAVE_STATE_PATH)
