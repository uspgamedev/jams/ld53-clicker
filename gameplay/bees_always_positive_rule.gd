extends ModifyRule

func _handle(effect: Effect, game_state: GameState):
	for bee_type in GameState.BeeType.values():
		var current_value = game_state.get_bee(bee_type)
		var lose_absolute: float = 1 if effect.traits.get("bee_to_lose") == bee_type else 0
		if lose_absolute > current_value:
			effect.traits.prevented = true
