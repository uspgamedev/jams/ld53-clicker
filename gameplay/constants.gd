extends Node


@export var pollen_per_tap_base := 1.0

@export var flower_pollen_per_sec_base := 0.0

@export var honey_maker_pollen_cost_base := 10.0
@export var honey_maker_honey_yield_base := 1.0

@export var expansion_honeycomb_wax_cost_base := 10.0
@export var expansion_honeycomb_wax_cost_progression_percent := 0.5

@export_category("Bee production")
@export var bee_population_cap_base := 6
@export var bee_population_cap_per_house := 6

@export_category("Commons skills")
@export var improved_tapping_bonus_percent := 0.05
@export var improved_honey_making_bonus_flat := 1

@export_category("Beesource production")
@export var beelivery_produce_rate := 2.0
@export var worker_bee_produce_rate := 0.1
@export var garden_bee_produce_rate := 0.1
@export var bee_cooker_produce_rate := 0.4
@export var motorbee_produce_rate := 15.0

@export_category("Beesource consumption")
@export var worker_bee_consume_rate := 1.0
@export var garden_bee_consume_rate := 0.5
@export var bee_cooker_consume_rate := 5
@export var motorbee_consume_rate := 1.0
@export var convert_price_honey := 50.0

@export_category("Combat")
@export var tap_attack_damage_base := 1
@export var bee_attack_damage_base := 20
@export var bee_speed_base := 1.0

@export_category("Threats")
@export var bear_honey_consume_rate := 2.0

var WIDTH = ProjectSettings.get_setting("display/window/size/viewport_width")
var HEIGHT = ProjectSettings.get_setting("display/window/size/viewport_height")
