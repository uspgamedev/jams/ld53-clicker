class_name Background
extends Control

@export var bg_tile_scn: PackedScene
@export var extend_threshold := .5
@export var scroll_damping := 50.0
@export var scroll_cooldown := 0.2

# Injected by gameplay
var game_state: GameState
var scroll_anchor: Control
var hive: Hive

# Publicily available
var scroll_min := 0.0
var scroll_max := 0.0

var _tapping := false
var _scroll_buffer := 0.0
var _timer

func _process(delta):
	if _timer and _timer.time_left > 0:
		_scroll_buffer = 0.0
		return
	else:
		_timer = null
		_tapping = false
	var last_tile := get_children().back() as TextureRect
	scroll_min = get_viewport_rect().size.x / 2
	scroll_max = last_tile.position.x + last_tile.size.x - scroll_min
	scroll_anchor.position.x = clampf(
		scroll_anchor.position.x + _scroll_buffer,
		scroll_min,
		scroll_max
	)
	_scroll_buffer = move_toward(_scroll_buffer, 0, scroll_damping * delta)
	# Increase BG if hive is catching up
	var hive_edge := hive.get_edge_position().x
	var threshold := scroll_max - last_tile.size.x * (1 - extend_threshold)
	if hive_edge > threshold:
		var new_tile := bg_tile_scn.instantiate()
		add_child(new_tile)


func _gui_input(event):
	if event is InputEventScreenDrag:
		scroll(event.relative.x)
	if event is InputEventScreenTouch:
		if event.pressed:
			_tapping = true
			_timer = get_tree().create_timer(scroll_cooldown)
		elif _tapping:
			var screen_center := get_viewport_rect().get_center()
			var camera_pos := get_viewport().get_camera_2d().global_position
			var tap = Effect.tap(event.position - (camera_pos - screen_center))
			Rulebook.resolve(tap, game_state)
			_tapping = false


func update_hive_width(width: int):
	print(width)


func scroll(diff: float):
	_scroll_buffer = -diff

