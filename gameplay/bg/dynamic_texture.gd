extends TextureRect

func _process(_delta):
	var img_size := texture.get_size()
	var vp_height := get_viewport_rect().size.y
	custom_minimum_size.x = img_size.x * vp_height / img_size.y
