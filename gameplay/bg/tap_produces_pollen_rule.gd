extends ModifyRule


func _handle(effect: Effect, _game_state: GameState):
	if effect.traits.has("tap"):
		effect.traits.pollen_add_base = Constants.pollen_per_tap_base
