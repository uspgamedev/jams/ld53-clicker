extends Control


const EFFECT_EMMITER = "EFFECT_EMMITER"

@export var debug_mode = false

@onready var game_state = $GameState

func _ready():
	$BG.game_state = game_state
	$BG.scroll_anchor = $ScrollAnchor
	$BG.hive = $HUD.get_hive()
	$ThreatDirector.bg = $BG
	$ThreatDirector.hive = $HUD.get_hive()
	$GameState.hive = $HUD.get_hive()
	$AutoSignalConnector.watch(self, self.resolve_effect)
	$ProtectionDirector.connect_threat_signals($ThreatDirector)
	var save_data = SaveState.load_data()
	propagate_call("_game_state_ready", [game_state, save_data])
	
	if not OS.has_feature("ld-build"):
		%DebugButton.visible = true
		if debug_mode:
			_on_debug_button_pressed()
			


func get_hud() -> HUD:
	return $HUD


func _notification(what):
	if what in [NOTIFICATION_WM_CLOSE_REQUEST, NOTIFICATION_WM_GO_BACK_REQUEST]:
		if debug_mode:
			return
		
		save()


func _physics_process(delta: float):
	resolve_effect(Effect.tick(delta))


func resolve_effect(effect: Effect):
	Rulebook.resolve(effect, game_state)


func _on_debug_button_pressed():
	$GameState.pollen = 10000
	$GameState.wax = 10000
	$GameState.honey = 10000
	$HUD/Overlay/Menu/SkillTree.propagate_call("activate")
	%DebugButton.disabled = true
	%DebugButton/Button.visible = true
	%DebugButton/Button2.visible = true
	Constants.bee_population_cap_base = 100
	debug_mode = true


func _on_button_pressed():
	if debug_mode:
		$ThreatDirector._spawn_threat()


func _on_button_2_pressed():
	if debug_mode:
		save()

func save():
	var save_data := {}
	propagate_call("_save_state", [save_data])
	SaveState.save_data(save_data)
