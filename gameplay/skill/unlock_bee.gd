class_name UnlockBeeRule
extends ModifyRule

@export var bee_type: GameState.BeeType

func _handle(effect: Effect, _game_state: GameState):
	if effect.traits.has("bee_to_spawn"):
		if effect.traits.bee_to_spawn == bee_type:
			effect.traits.bee_unlocked = true
