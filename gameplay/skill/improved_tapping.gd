class_name ImprovedTapping
extends ModifyRule

func _handle(effect: Effect, _game_state: GameState):
	if effect.traits.has("tap"):
		effect.stack_trait(
			"pollen_add_bonus_percent",
			Constants.improved_tapping_bonus_percent
		)
