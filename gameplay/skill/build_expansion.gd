extends Node

signal effect_created(effect: Effect)

@export var expansion_scn: PackedScene

func _ready():
	add_to_group("EFFECT_EMITTER")


func _on_skill_activated():
	effect_created.emit(Effect.expand_hive(expansion_scn.instantiate(), 0))
