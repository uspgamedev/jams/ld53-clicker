class_name ImprovedConversion
extends ModifyRule

@export var beesource: GameState.Beesource
@export var bonus_flat := 0.0
@export var bonus_percent := 0.0
@export var bonus_exp := 0

func _handle(effect: Effect, _game_state: GameState):
	if effect.traits.has("transform_beesource"):
		var beesource_name := GameState.VALID_RESOURCE_NAMES[beesource]
		effect.stack_trait(
			"%s_add_bonus_flat" % beesource_name,
			bonus_flat
		)
		effect.stack_trait(
			"%s_add_bonus_percent" % beesource_name,
			bonus_percent
		)
		effect.stack_trait(
			"%s_add_bonus_exp" % beesource_name,
			bonus_exp
		)
