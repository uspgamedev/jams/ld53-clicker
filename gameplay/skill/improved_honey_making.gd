class_name ImprovedHoneyMaking
extends ModifyRule

func _handle(effect: Effect, _game_state: GameState):
	if effect.traits.has("honey_making"):
		effect.stack_trait(
			"honey_add_bonus_flat",
			Constants.improved_honey_making_bonus_flat
		)
