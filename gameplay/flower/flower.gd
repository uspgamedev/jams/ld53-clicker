extends Sprite2D

@export var sprites : Array[Texture]

@export var WIDTH := 350
@export var MAX_SHIFT_X := 10 
@export var MAX_SHIFT_Y := 150 
@export var MAX_ROTATION_DEGREES := 3.5

func spawn(position_index: int):
	var i = randi_range(0, sprites.size()-1)
	self.texture = sprites[i]
	self.position.y = Constants.HEIGHT + MAX_SHIFT_Y * randf_range(0.0, 1.0)
	self.position.x = scale.x * WIDTH * (1 + position_index) + MAX_SHIFT_X * randf_range(-1.0, 1.0)
	self.rotation = randf_range(-1.0, 1.0) * deg_to_rad(MAX_ROTATION_DEGREES)
	
	
