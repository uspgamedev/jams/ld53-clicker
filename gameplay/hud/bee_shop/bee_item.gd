@tool
extends NinePatchRect

@export var icon : Texture = null
@export var bee_type : GameState.BeeType
@export_category("Description")
@export var bee_name : String = "MotorBee"
@export var description : String = "Vruuum Vruuum"
@export var rate : String = "∞ pollen/s"

@export_category("Price")
@export var price := 20.0
@export var beesource : GameState.Beesource
@export var beesource_icon : Texture = preload("res://assets/pollen_icon.png")

var _game_state: GameState

func _ready():
	$Icon.texture = icon
	%Name.text = "[font_size=25][b]%s[/b][/font_size]" % bee_name
	%Description.text = "[font_size=18][color=dark gray]%s[/color][/font_size]" % description
	%Rate.text = "[color=cyan]%s[/color]" % rate
	%Price.text = "\n[center][img=20x20]%s[/img][color=cyan] %d[/color][/center]" % [beesource_icon.resource_path, price]
	
func _game_state_ready(game_state: GameState, _save_data):
	_game_state = game_state
	game_state.bees_changed.connect(change_quantity)

func _process(_delta):
	if not Engine.is_editor_hint():
		var produce_effect := Rulebook.preview(
			Effect.produce_bee(bee_type, beesource, price),
			_game_state
		)
		var spawn_effect := Rulebook.preview(
			Effect.spawn_bee(bee_type),
			_game_state
		)
		%BuyButton.disabled = produce_effect.is_prevented()
		%BuyButton.modulate = Color(1, 1, 1, 0.2) \
			if produce_effect.is_prevented() \
			else Color.WHITE
		visible = not spawn_effect.is_prevented()

func _on_buy_button_pressed():
	var fx := Rulebook.resolve(
		Effect.produce_bee(bee_type, beesource, price),
		_game_state
	)
	if not fx.is_prevented():
		$BirthSFX.play()

func change_quantity(_bee_type: GameState.BeeType, value: int):
	if self.bee_type == _bee_type:
		%Quantity.text = "[center][color=cyan]qty: %d[/color][/center]" % [value]
	
