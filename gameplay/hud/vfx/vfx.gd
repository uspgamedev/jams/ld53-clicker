extends Control

@export var pollen_bling_scn: PackedScene

func add_pollen_bling(pos: Vector2, value: float):
	var pollen_bling := pollen_bling_scn.instantiate() as ResourceBling
	pollen_bling.position = pos
	pollen_bling.value = value
	add_child(pollen_bling)
