class_name ResourceBling
extends Node2D

@export var value := 100.0
@export var distance := 200
@export var duration := 1.0

func _ready():
	%Value.text = "+%.1f" % value
	var target_pos := position + Vector2.UP * distance
	var tween := create_tween()
	tween.tween_property(self, "position", target_pos, duration)
	tween \
		.parallel() \
		.set_ease(Tween.EASE_IN) \
		.set_trans(Tween.TRANS_CUBIC) \
		.tween_property(self, "modulate", Color.TRANSPARENT, duration)
	await tween.finished
	queue_free()
