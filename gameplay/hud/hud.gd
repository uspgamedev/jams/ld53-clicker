class_name HUD
extends Control

@onready var counter_setters = {
	GameState.Beesource.NONE: func (_value): 
		return,
	GameState.Beesource.POLLEN: func (value):
		%TopBar.get_pollen_counter().target_value = value,
	GameState.Beesource.FLOWERS: func (_value): 
		return,
	GameState.Beesource.HONEY: func (value):
		%TopBar.get_honey_counter().target_value = value,
	GameState.Beesource.WAX: func (value):
		%TopBar.get_wax_counter().target_value = value
}

func get_hive() -> Hive:
	return $Hive


func _on_beesource_changed(beesource: GameState.Beesource, value: float):
	counter_setters[beesource].call(value)


func _on_effect_resolved(effect: Effect):
	match effect.traits:
		{"tap": true, "tap_position": var pos, "pollen_add_absolute": var value, ..}:
			%VFX.add_pollen_bling(pos, value)


func _on_toggle_menu():
	%Menu.visible = not %Menu.visible
	if %Menu.visible:
		$OpenMenuSFX.play()
	else:
		$CloseMenuSFX.play()


func _on_shop_tab_pressed():
	%BeeShop.visible = true
	%SkillTree.visible = false


func _on_skill_tab_pressed():
	%BeeShop.visible = false
	%SkillTree.visible = true
