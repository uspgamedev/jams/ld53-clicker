extends Control

signal toggle_menu

func get_pollen_counter() -> BeesourceCounter:
	return %PollenCounter


func get_honey_counter() -> BeesourceCounter:
	return %HoneyCounter


func get_wax_counter() -> BeesourceCounter:
	return %WaxCounter


func _on_toggle_menu():
	toggle_menu.emit()
