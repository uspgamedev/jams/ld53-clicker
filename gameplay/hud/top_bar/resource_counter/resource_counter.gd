class_name BeesourceCounter
extends Control

@export var icon: Texture = preload("res://icon.svg")
@export var count_speed := 100.0

var target_value := 0.0
var current_value := 0.0

func _ready():
	%Icon.texture = icon

func _process(delta):
	current_value = move_toward(
		current_value,
		target_value,
		count_speed * delta
	)
	%ResourceValue.text = "%d" % int(current_value)
