extends MarginContainer


func _on_pressed():
	SaveState.clear_data()
	var main_scn_path = ProjectSettings.get_setting("application/run/main_scene")
	get_tree().change_scene_to_file(main_scn_path)
