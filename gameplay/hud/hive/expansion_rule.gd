extends ApplyRule

var hive: Hive

func _handle(effect: Effect, _game_state: GameState):
	if effect.traits.has("expansion"):
		var expansion := effect.traits.expansion as Expansion
		hive.add_hive_expansion(expansion)
