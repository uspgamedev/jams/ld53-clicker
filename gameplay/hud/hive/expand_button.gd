extends Control

signal effect_created(effect: Effect)

@export var expansion_scn: PackedScene

var _expansion: Expansion
var _game_state: GameState

func _game_state_ready(game_state: GameState, _save_data):
	_game_state = game_state

func _process(_delta):
	var expand_effect := Effect.expand_hive(
		_update_expansion(),
		Constants.expansion_honeycomb_wax_cost_base,
	)
	Rulebook.preview(expand_effect, _game_state)
	var cost = expand_effect.traits.wax_lose_absolute
	%WaxCost.text = "-%d" % cost
	%Button.disabled = expand_effect.is_prevented()


func _update_expansion():
	if not _expansion:
		_expansion = expansion_scn.instantiate()
	return _expansion

func _on_pressed():
	var fx := Effect.expand_hive(
		_update_expansion(),
		Constants.expansion_honeycomb_wax_cost_base,
	)
	effect_created.emit(fx)
	_expansion = null
	if not fx.is_prevented():
		$ExpandSFX.play()
