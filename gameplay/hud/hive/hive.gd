class_name Hive
extends Control

@export var starting_expansions: Array[PackedScene]
@export var first_column := -4

@onready var honeycombs := $Honeycombs as TileMap

var _game_state: GameState

func _ready():
	$ExpansionRule.hive = self


func _game_state_ready(game_state: GameState, save_data):
	_game_state = game_state
	if save_data:
		for expansion_scn_path in save_data.expansions:
			var expansion_scn := load(expansion_scn_path) as PackedScene
			var expansion := expansion_scn.instantiate()
			add_hive_expansion(expansion)
	else:
		for expansion_scn in starting_expansions:
			var expansion := expansion_scn.instantiate()
			add_hive_expansion(expansion)


func _save_state(save_data: Dictionary):
	save_data.expansions = []
	for expansion in get_tree().get_nodes_in_group("EXPANSION"):
		if expansion is Expansion:
			var scene_path := expansion.scene_file_path
			save_data.expansions.append(scene_path)


func _process(_delta):
	var anchor_pos := $Honeycombs/FirstAnchor.position as Vector2
	var hive_width := 0
	for child in honeycombs.get_children():
		if child.is_in_group("ANCHOR") and child.get_child_count() > 0:
			var expansion := child.get_child(0) as Expansion
			if expansion:
				child.position = anchor_pos
				anchor_pos = child.position + expansion.get_next_anchor()
				hive_width += expansion.width
	$ExpandButton.position = honeycombs.position + anchor_pos * honeycombs.scale
	for i in hive_width:
		var cell_pos := Vector2i(first_column + i, -1)
		if not honeycombs.get_cell_tile_data(0, cell_pos):
			var variation := randi_range(0, 3)
			honeycombs.set_cell(0, cell_pos, 0, Vector2i(variation, 0))


func add_hive_expansion(expansion: Expansion):
	var anchor := Node2D.new()
	expansion.game_state = _game_state
	anchor.add_to_group("ANCHOR")
	anchor.add_child(expansion)
	honeycombs.add_child(anchor)


func get_edge_position() -> Vector2:
	return $ExpandButton.global_position
