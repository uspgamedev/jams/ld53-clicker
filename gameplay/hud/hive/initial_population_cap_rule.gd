extends ModifyRule


func _handle(effect: Effect, _game_state: GameState):
	if effect.traits.has("bee_to_produce"):
		effect.stack_trait(
			"bee_population_cap",
			Constants.bee_population_cap_base
		)
