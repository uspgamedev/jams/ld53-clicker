extends Control


signal effect_created(effect: Effect)

func _on_pressed():
	effect_created.emit(Effect.new({
		honey_making = true,
		pollen_lose_base = Constants.honey_maker_pollen_cost_base,
		honey_add_base = Constants.honey_maker_honey_yield_base
	}))
