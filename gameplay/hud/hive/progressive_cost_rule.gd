extends ModifyRule

const GROUP := "EXPANSION_HONEYCOMB"

func _handle(effect: Effect, _game_state: GameState):
	if effect.traits.has("expansion"):
		var expansion := effect.traits.expansion as Expansion
		if expansion.is_in_group(GROUP):
			var count := get_tree().get_nodes_in_group(GROUP).size()
			effect.stack_trait(
				"wax_lose_bonus_percent",
				Constants.expansion_honeycomb_wax_cost_progression_percent
			)
			effect.stack_trait("wax_lose_bonus_exp", count)
