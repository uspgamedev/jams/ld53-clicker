extends Button

signal effect_created(effect: Effect)

@export var expansion: Expansion
@export var pollen_cost: Label
@export var honey_yield: Label


func _process(_delta):
	var game_state = expansion.game_state
	if not game_state: return
	var effect := Rulebook.preview(Effect.new({
		honey_making = true,
		pollen_lose_base = Constants.honey_maker_pollen_cost_base,
		honey_add_base = Constants.honey_maker_honey_yield_base
	}), game_state)
	if effect.is_prevented():
		modulate = Color(1, 1, 1, 0.2)
	else:
		modulate = Color.WHITE
	pollen_cost.text = "-%.0f" % effect.traits.pollen_lose_absolute
	honey_yield.text = "+%.0f" % effect.traits.honey_add_absolute


func _on_pressed():
	var fx = Effect.new({
		honey_making = true,
		pollen_lose_base = Constants.honey_maker_pollen_cost_base,
		honey_add_base = Constants.honey_maker_honey_yield_base
	})
	effect_created.emit(fx)
	if not fx.is_prevented():
		$MakeHoneySFX.play()
