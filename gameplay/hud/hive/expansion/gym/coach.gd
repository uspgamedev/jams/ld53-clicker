extends Sprite2D

@export var variation := 100

func _ready():
	var tween := create_tween().set_trans(Tween.TRANS_SINE).set_ease(Tween.EASE_IN_OUT)
	tween.tween_property(self, "position", position + Vector2.UP * variation, 1.0)
	tween.tween_property(self, "position", position, 1.0)
	tween.set_loops()
