extends Button

signal effect_created(effect: Effect)

@export var expansion: Expansion
@export var honey_cost: Label


func _process(_delta):
	var game_state = expansion.game_state
	if not game_state: return
	var effect := Rulebook.preview(Effect.new({
		convert = true,
		honey_lose_base = Constants.convert_price_honey,
		bee_to_lose = GameState.BeeType.Beelivery,
		bee_to_add = GameState.BeeType.BeeFy
	}), game_state)
	if effect.is_prevented():
		modulate = Color(1, 1, 1, 0.2)
	else:
		modulate = Color.WHITE
#	honey_cost.text = "-%.0f" % effect.traits.pollen_lose_absolute


func _on_pressed():
	effect_created.emit(Effect.new({
		convert = true,
		price = Constants.convert_price_honey,
		bee_to_lose = GameState.BeeType.Beelivery,
		bee_to_add = GameState.BeeType.BeeFy
	}))
