class_name SkillTree
extends Control

@export var scroll_damping := 50.0

@onready var hex_grid := %HexGrid
@onready var details := %Details as RichTextLabel
@onready var learn_button := %LearnButton as Button

var _scroll_buffer := Vector2.ZERO
var _selected_skill: HexNode
var _game_state: GameState

func _ready():
	for hex in hex_grid.get_children():
		if hex is HexNode:
			hex.selected.connect(self._on_hex_selected)


func _game_state_ready(game_state: GameState, save_data):
	_game_state = game_state
	if save_data:
		for skill_path in save_data.skills:
			var skill_node := hex_grid.get_node(skill_path)
			skill_node.activate()


func _save_state(save_data: Dictionary):
	var skills := []
	for node in hex_grid.get_children():
		if node is HexNode and node.active:
			skills.append(hex_grid.get_path_to(node))
	save_data.skills = skills


func _process(delta):
	hex_grid.position += _scroll_buffer
	_scroll_buffer = _scroll_buffer.move_toward(
		Vector2.ZERO,
		scroll_damping * delta
	)
	if _selected_skill:
		details.text = "[b]%s[/b]\n%s" % [
			_selected_skill.skill_name,
			_selected_skill.description
		]
		learn_button.text = "LEARN SKILL (-%d honey)" % _selected_skill.honey_cost
		learn_button.disabled = not _selected_skill.available \
								or _selected_skill.active \
								or _selected_skill.honey_cost > _game_state.honey
	else:
		details.text = "Select a [b]skill[/b] for more information!"


func _gui_input(event):
	if event is InputEventScreenDrag:
		_scroll_buffer = event.relative


func _on_hex_selected(hex: HexNode):
	if _selected_skill:
		_selected_skill.highlight = false
	_selected_skill = hex
	_selected_skill.highlight = true


func _on_learn_button_pressed():
	if _selected_skill:
		var effect := Effect.new({
			learn_skill = true,
			honey_lose_base = _selected_skill.honey_cost
		})
		Rulebook.resolve(effect, _game_state)
		if not effect.traits.has("prevented"):
			_selected_skill.activate()
			$LearnSFX.play()
