@tool
class_name HexNode
extends Node2D

signal selected(hex: HexNode)
signal activated

@export var skill_name := "Skill"

@export_category("Details")
@export_multiline var description := "Is very cool"
@export var honey_cost := 10
@export var dependencies: Array[NodePath]

@export_category("Icons")
@export var icon: Texture2D
@export var icon_offset: Vector2 = Vector2.ZERO
@export var icon_scale: float = 1.0
@export var show_plus_icon: bool

@export_category("Internals")
@export var edge_scn: PackedScene
@export var active := false

@export var unavailable_color: Color
@export var available_color: Color
@export var active_color: Color

var highlight := false
var available := false

var _edges: Array[Line2D] = []

func _process(_delta):
	# Set icons
	$MainIcon.texture = icon
	$MainIcon.offset = icon_offset
	$MainIcon.scale = Vector2.ONE * icon_scale
	$PlusIcon.visible = show_plus_icon
	# Generate edges to dependencies
	while _edges.size() < dependencies.size():
		var edge := edge_scn.instantiate()
		_edges.append(edge)
		add_child(edge)
	while _edges.size() > dependencies.size():
		_edges.back().queue_free()
		_edges.pop_back()
	for index in dependencies.size():
		var dependency_path := dependencies[index]
		var dependency := get_node_or_null(dependency_path)
		if dependency is HexNode:
			var edge := _edges[index]
			var start := get_nearest_vertex(dependency.position)
			var end = dependency.get_nearest_vertex(position)
			edge.set_point_position(0, start - position)
			edge.set_point_position(1, end - position)
	# Determine colors
	$Border.default_color = unavailable_color
	available = false
	var available_edges: Array[Line2D] = []
	for index in dependencies.size():
		var dependency_path := dependencies[index]
		var dependency := get_node_or_null(dependency_path) as HexNode
		var edge := _edges[index]
		if dependency.active:
			available = true
			edge.default_color = available_color
			available_edges.append(edge)
			$Border.default_color = available_color
		else:
			edge.default_color = unavailable_color
	if active:
		$Border.default_color = active_color
		for edge in available_edges:
			edge.default_color = active_color
	# Handle highlight
	var scale_border := 1.0
	if highlight:
		scale_border = 1.2
	$Border.scale = Vector2.ONE * scale_border

func get_nearest_vertex(pos: Vector2) -> Vector2:
	var min_dist := INF
	var nearest: Vector2
	for vertex in $Border.points:
		var dist := pos.distance_squared_to(position + vertex * $Border.scale)
		if dist < min_dist:
			min_dist = dist
			nearest = vertex * $Border.scale
	return position + nearest


func activate():
	active = true
	for child in get_children():
		if child is Rule:
			child.enabled = true
	activated.emit()


func _on_pressed():
	selected.emit(self)
	$SelectSFX.play()
