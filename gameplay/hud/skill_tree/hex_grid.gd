@tool
extends TileMap

func _process(_delta):
	for child in get_children():
		if child is HexNode:
			var map_pos := local_to_map(child.position)
			child.position = map_to_local(map_pos)
