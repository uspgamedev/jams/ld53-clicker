class_name BGM
extends Node

@export var fade_speed := 40

enum Mood {
	SAFE,
	DANGER
}

const TARGETS := [
	{
		Beats = 0,
		Song = 0,
		Bumble = -80
	},
	{
		Beats = 0,
		Song = -80,
		Bumble = 0
	},
]

var mood := Mood.SAFE

func _process(delta):
	for track in get_children():
		if track is AudioStreamPlayer:
			var target = TARGETS[mood][track.name]
			track.volume_db = move_toward(
				track.volume_db,
				target,
				fade_speed * delta
			)


func set_safe_mood():
	mood = Mood.SAFE


func set_danger_mood():
	mood = Mood.DANGER
